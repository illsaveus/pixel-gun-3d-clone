﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Photon.Pun;

public class Shooting : MonoBehaviourPunCallbacks
{
    public Camera fps_Camera;
    public GameObject hitEffectPrefab;

    [Header("Health")]
    public float startHealth = 100f;
    private float health;
    public Image healthBar;

    private Animator anim;

    private void Start()
    {
        health = startHealth;
        healthBar.fillAmount = health / startHealth;
        anim = GetComponent<Animator>();
    }

    public void Fire()
    {
        Ray ray = fps_Camera.ViewportPointToRay(new Vector3(0.5f, 0.5f));
        RaycastHit _hit;

        if(Physics.Raycast(ray, out _hit, 100))
        {
            Debug.Log($"hit {_hit.collider.gameObject.name}");
            photonView.RPC("CreateHitEffect", RpcTarget.All, _hit.point);

            if(_hit.collider.gameObject.CompareTag("Player") && !_hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
            {
                Debug.Log("hit a player");
                _hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 10f);
            }
        }
    }

    [PunRPC]
    public void CreateHitEffect(Vector3 pos)
    {
        GameObject hiteffectGameobject = Instantiate(hitEffectPrefab, pos, Quaternion.identity);
        Destroy(hiteffectGameobject, 0.5f);
    }

    [PunRPC]
    public void TakeDamage(float _damage, PhotonMessageInfo info)
    {
        health -= _damage;
        Debug.Log($"taking damage now at: {health} health");
        healthBar.fillAmount = health / startHealth;

        if(health <= 0f)
        {
            Die();
            Debug.Log(info.Sender.NickName + " killed " + info.photonView.Owner.NickName);
        }
    }

    private void Die()
    {
        if(photonView.IsMine)
        {
            anim.SetBool("isDead", true);
            StartCoroutine(Respawn());
        }
    }

    private IEnumerator Respawn()
    {
        GameObject respawnText = GameObject.Find("RespawnText");


        float respawnTime = 8f;
        while(respawnTime > 0)
        {
            yield return new WaitForSeconds(1f);
            respawnTime--;

            transform.GetComponent<PlayerMovementController>().enabled = false; //deactivate movement
            respawnText.GetComponent<TMP_Text>().text = "You have died. Respawning in .. " + respawnTime.ToString(".00");
        }

        //respawn player
        anim.SetBool("isDead", false);
        respawnText.GetComponent<TMP_Text>().text = "";

        int randomPoint = Random.Range(-20, 20);
        transform.position = new Vector3(randomPoint, 0f, randomPoint);
        transform.GetComponent<PlayerMovementController>().enabled = true;

        photonView.RPC("RegainHealth", RpcTarget.AllBuffered);
    }

    [PunRPC]
    public void RegainHealth()
    {
        health = startHealth;
        healthBar.fillAmount = health / startHealth;
    }
}
