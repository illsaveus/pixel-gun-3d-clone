﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerMovementController : MonoBehaviour
{
    public Joystick joystick;
    public FixedTouchField fixedTouchField;

    private RigidbodyFirstPersonController rbController;
    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        rbController = GetComponent<RigidbodyFirstPersonController>();
        anim = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        rbController.joystickInputAxis.x = joystick.Horizontal;
        rbController.joystickInputAxis.y = joystick.Vertical;
        rbController.mouseLook.lookInputAxis = fixedTouchField.TouchDist;

        anim.SetFloat("Horizontal", joystick.Horizontal);
        anim.SetFloat("Vertical", joystick.Vertical);

        if(Mathf.Abs(joystick.Horizontal) > 0.8f || Mathf.Abs(joystick.Vertical) > 0.8f)
        {
            rbController.movementSettings.ForwardSpeed = 16;
            anim.SetBool("isRunning", true);
        }
        else
        {
            rbController.movementSettings.ForwardSpeed = 8;
            anim.SetBool("isRunning", false);
        }
    }
}
