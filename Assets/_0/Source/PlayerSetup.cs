﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerSetup : MonoBehaviourPunCallbacks
{
    public GameObject[] FPS_Hands_ChildGameObjects, Soldier_ChildGameObjects;
    public GameObject playerUIPrefab;
    public Camera FPS_cam;

    private PlayerMovementController playerMovementController;
    private Animator anim;
    private Shooting shooter;

    // Start is called before the first frame update
    void Start()
    {
        playerMovementController = GetComponent<PlayerMovementController>();
        anim = GetComponent<Animator>();
        shooter = GetComponent<Shooting>();

        if(photonView.IsMine)
        {
            //activate fps hands, deactivate soldier model
            foreach(GameObject gameObject in FPS_Hands_ChildGameObjects)
            {
                gameObject.SetActive(true);
            }

            foreach (GameObject gameObject in Soldier_ChildGameObjects)
            {
                gameObject.SetActive(false);
            }

            //instantiate player ui
            GameObject playerUIgo = Instantiate(playerUIPrefab);
            playerMovementController.joystick = playerUIgo.transform.Find("Fixed Joystick").GetComponent<Joystick>();
            playerMovementController.fixedTouchField = playerUIgo.transform.Find("Rotation Touch Field").GetComponent<FixedTouchField>();
            playerUIgo.transform.Find("Fire Button").GetComponent<Button>().onClick.AddListener(() => shooter.Fire());
            FPS_cam.enabled = true;
            anim.SetBool("isSoldier", false);
        }
        else
        {
            //deactivate fps hands, activate soldier model
            foreach (GameObject gameObject in FPS_Hands_ChildGameObjects)
            {
                gameObject.SetActive(false);
            }

            foreach (GameObject gameObject in Soldier_ChildGameObjects)
            {
                gameObject.SetActive(true);
            }

            playerMovementController.enabled = false;
            GetComponent<RigidbodyFirstPersonController>().enabled = false;
            FPS_cam.enabled = false;
            anim.SetBool("isSoldier", true);
        }
    }

}
