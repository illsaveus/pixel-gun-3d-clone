﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using SimpleDemo;

namespace SimpleDemo
{
    public class PlayerNameInputManager : MonoBehaviour
    {
        public void SetPlayerName(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                Debug.Log("player name is empty");
                return;
            }

            PhotonNetwork.NickName = name;
        }
    }
}