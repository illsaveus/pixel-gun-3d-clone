﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using SimpleDemo;

namespace SimpleDemo
{
    public class TakingDamage : MonoBehaviourPunCallbacks
    {
        public Image healthBar;
        private float health;
        public float startHealth = 100f;

        // Start is called before the first frame update
        void Start()
        {
            health = startHealth;
            healthBar.fillAmount = health / startHealth;
        }

        private void Update()
        {

        }

        [PunRPC]
        public void TakeDamage(float damage)
        {
            health -= damage;

            if (health <= 0f)
            {
                Die();
            }

            Debug.Log(health);
            healthBar.fillAmount = health / startHealth;
        }

        private void Die()
        {
            //this will be called on all clients so you need to make sure you only die on the current user
            if (photonView.IsMine)
            {
                PixelGunGameManager.Instance.LeaveRoom();
            }
        }
    }
}