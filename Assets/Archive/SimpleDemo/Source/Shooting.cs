﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using SimpleDemo;

namespace SimpleDemo
{
    public class Shooting : MonoBehaviour
    {
        public Camera fpsCamera;

        public float fireRate = 0.1f;
        private float fireTimer;

        private void Update()
        {

            if (fireTimer < fireRate)
            {
                fireTimer += Time.deltaTime;
            }


            if (Input.GetButton("Fire1") && fireTimer > fireRate)
            {
                fireTimer = 0.0f;

                RaycastHit hit;
                Ray ray = fpsCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f)); //send a ray from the center of the viewport of the player cam

                if (Physics.Raycast(ray, out hit, 100))
                {
                    Debug.Log("hit " + hit.collider.gameObject.name);

                    //make sure you hit a player but also not yourself
                    if (hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
                    {
                        //TakeDamage is the name of the function to be called. RpcTarget.all means everyone in the room will get this call (all buffered means players who join later will also get this call)
                        //10f is the parament that method takes
                        hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 10f);
                    }
                }
            }
        }
    }
}