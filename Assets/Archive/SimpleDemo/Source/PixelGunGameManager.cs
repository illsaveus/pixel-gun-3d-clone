﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.SceneManagement;
using SimpleDemo;

namespace SimpleDemo
{
    public class PixelGunGameManager : MonoBehaviourPunCallbacks
    {
        public GameObject playerPrefab;

        public static PixelGunGameManager Instance;

        private void Awake()
        {
            if (Instance != null)
            {
                Destroy(this.gameObject);
            }
            else
            {
                Instance = this;
            }
        }

        // Start is called before the first frame update
        void Start()
        {
            if (PhotonNetwork.IsConnected)
            {
                int randomPoint = Random.Range(-20, 20);

                //instatiate player on PUN
                PhotonNetwork.Instantiate(playerPrefab.name, new Vector3(randomPoint, 0, randomPoint), Quaternion.identity);
            }
        }

        // Update is called once per frame
        void Update()
        {

        }

        //check if players in the same room can join this game scene

        public override void OnJoinedRoom()
        {
            base.OnJoinedRoom();
            Debug.Log($"{PhotonNetwork.NickName} joined room {PhotonNetwork.CurrentRoom.Name}");
        }

        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            base.OnPlayerEnteredRoom(newPlayer);
            Debug.Log($"{newPlayer.NickName} joined room {PhotonNetwork.CurrentRoom.Name} : Player count = {PhotonNetwork.CurrentRoom.PlayerCount}");
        }

        public override void OnLeftRoom()
        {
            base.OnLeftRoom();
            SceneManager.LoadScene("GameLauncherScene");
        }

        public void LeaveRoom()
        {
            PhotonNetwork.LeaveRoom();
        }
    }
}