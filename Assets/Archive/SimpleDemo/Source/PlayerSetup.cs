﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;
using SimpleDemo;

namespace SimpleDemo
{
    public class PlayerSetup : MonoBehaviourPunCallbacks
    {
        public GameObject fpsCam;
        public TMP_Text playerName;

        // Start is called before the first frame update
        void Start()
        {
            //checks if i own this photon view
            if (photonView.IsMine)
            {
                transform.GetComponent<MovementController>().enabled = true;
                fpsCam.GetComponent<Camera>().enabled = true;
            }
            else
            {
                transform.GetComponent<MovementController>().enabled = false;
                fpsCam.GetComponent<Camera>().enabled = false;
            }

            SetPlayerUI();
        }

        private void SetPlayerUI()
        {
            playerName.text = photonView.Owner.NickName;
        }
    }
}