﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleDemo;

namespace SimpleDemo
{
    public class MovementController : MonoBehaviour
    {
        public float speed = 5f;
        private Vector3 velocity;
        private Rigidbody rb;
        public float lookSensitivity = 3f;
        private Vector3 rotation = Vector3.zero;
        public GameObject fpsCamera;
        private float camUpDownRotation;
        private float currentCamUpDnRotation = 0f;

        // Start is called before the first frame update
        void Start()
        {
            rb = GetComponent<Rigidbody>();
        }

        // Update is called once per frame
        void Update()
        {
            float xMovement = Input.GetAxis("Horizontal");
            float zMovement = Input.GetAxis("Vertical");

            Vector3 movementHorizontal = transform.right * xMovement;
            Vector3 movementVertical = transform.forward * zMovement;

            Vector3 movementVelocity = (movementHorizontal + movementVertical).normalized * speed;

            Move(movementVelocity);

            float yRotation = Input.GetAxis("Mouse X");
            Vector3 rotationVector = new Vector3(0, yRotation, 0) * lookSensitivity;

            Rotate(rotationVector);

            float cameraUpDownRotation = Input.GetAxis("Mouse Y") * lookSensitivity;
            RotateCamera(cameraUpDownRotation);
        }

        private void FixedUpdate()
        {
            if (velocity != Vector3.zero)
            {
                rb.MovePosition(rb.position + velocity * Time.fixedDeltaTime);
            }

            rb.MoveRotation(rb.rotation * Quaternion.Euler(rotation));

            if (fpsCamera != null)
            {
                currentCamUpDnRotation -= camUpDownRotation;
                currentCamUpDnRotation = Mathf.Clamp(currentCamUpDnRotation, -85, 85);
                fpsCamera.transform.localEulerAngles = new Vector3(currentCamUpDnRotation, 0, 0);
            }
        }

        private void RotateCamera(float updownRotation)
        {
            camUpDownRotation = updownRotation;
        }

        private void Rotate(Vector3 rotationVector)
        {
            rotation = rotationVector;
        }

        private void Move(Vector3 movementVelocity)
        {
            velocity = movementVelocity;
        }

    }
}
