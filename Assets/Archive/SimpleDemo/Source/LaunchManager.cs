﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using SimpleDemo;

namespace SimpleDemo
{
    public class LaunchManager : MonoBehaviourPunCallbacks
    {
        public GameObject EnterGamePanel, ConnectionStatusPanel, LobbyPanel;

        #region - Public Functions- 

        private void Awake()
        {
            PhotonNetwork.AutomaticallySyncScene = true; //all clients in the same room sync their level automatically, as master client.
        }
        private void Start()
        {
            EnterGamePanel.SetActive(true);
            ConnectionStatusPanel.SetActive(false);
            LobbyPanel.SetActive(false);
        }

        public void ConnectToPUN()
        {
            if (!PhotonNetwork.IsConnected)
            {
                PhotonNetwork.ConnectUsingSettings();
                ConnectionStatusPanel.SetActive(true);
                EnterGamePanel.SetActive(false);
            }
        }
        public void JoinRandomRoom()
        {
            PhotonNetwork.JoinRandomRoom();
        }
        #endregion

        #region - PUN CallBacks -

        public override void OnConnectedToMaster()
        {
            Debug.Log($"{PhotonNetwork.NickName} is connected to pun");
            LobbyPanel.SetActive(true);
            EnterGamePanel.SetActive(false);
        }


        public override void OnConnected()
        {
            base.OnConnected();
            Debug.Log("Connected to internet");
        }

        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            base.OnJoinRandomFailed(returnCode, message);
            Debug.Log(message);
            CreateAndJoinRoom();
        }

        public override void OnJoinedRoom()
        {
            base.OnJoinedRoom();
            Debug.Log($"{PhotonNetwork.NickName} has joined room {PhotonNetwork.CurrentRoom.Name}");
            PhotonNetwork.LoadLevel("GameScene"); //once master client has created this scene, the other clients will load into the scene automatically as soon as they join, this line won't even be called for clients
        }

        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            base.OnPlayerEnteredRoom(newPlayer);
            Debug.Log($"{newPlayer.NickName} joined room {PhotonNetwork.CurrentRoom.Name}. Player count is now {PhotonNetwork.CurrentRoom.PlayerCount}");
        }

        #endregion

        #region - Private Methods -
        private void CreateAndJoinRoom()
        {
            string randomRoomName = "Room " + Random.Range(0, 100000);

            RoomOptions roomOptions = new RoomOptions();
            roomOptions.IsOpen = true;
            roomOptions.IsVisible = true;
            roomOptions.MaxPlayers = 20;

            PhotonNetwork.CreateRoom(randomRoomName, roomOptions);
        }
        #endregion
    }
}
